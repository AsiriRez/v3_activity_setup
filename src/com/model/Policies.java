package com.model;

public class Policies {
	
	public String ActivityProgramName_Policies;
	
	public String TravelD_from;
	public String TravelD_to;
	public String Can_Apply_By;
	public String Can_Less_than;
	public String Can_buffer;
	public String Standard_Can_BasedOn;
	public String Standard_Can_Value;
	public String No_Show_Can_BasedOn;
	public String No_Show_Can_Value;
	
	public String getActivityProgramName_Policies() {
		return ActivityProgramName_Policies;
	}
	public void setActivityProgramName_Policies(String activityProgramName_Policies) {
		ActivityProgramName_Policies = activityProgramName_Policies;
	}
	
	public String getTravelD_from() {
		return TravelD_from;
	}
	public void setTravelD_from(String travelD_from) {
		TravelD_from = travelD_from;
	}
	public String getTravelD_to() {
		return TravelD_to;
	}
	public void setTravelD_to(String travelD_to) {
		TravelD_to = travelD_to;
	}
	public String getCan_Apply_By() {
		return Can_Apply_By;
	}
	public void setCan_Apply_By(String can_Apply_By) {
		Can_Apply_By = can_Apply_By;
	}
	public String getCan_Less_than() {
		return Can_Less_than;
	}
	public void setCan_Less_than(String can_Less_than) {
		Can_Less_than = can_Less_than;
	}
	public String getCan_buffer() {
		return Can_buffer;
	}
	public void setCan_buffer(String can_buffer) {
		Can_buffer = can_buffer;
	}
	public String getStandard_Can_BasedOn() {
		return Standard_Can_BasedOn;
	}
	public void setStandard_Can_BasedOn(String standard_Can_BasedOn) {
		Standard_Can_BasedOn = standard_Can_BasedOn;
	}
	public String getStandard_Can_Value() {
		return Standard_Can_Value;
	}
	public void setStandard_Can_Value(String standard_Can_Value) {
		Standard_Can_Value = standard_Can_Value;
	}
	public String getNo_Show_Can_BasedOn() {
		return No_Show_Can_BasedOn;
	}
	public void setNo_Show_Can_BasedOn(String no_Show_Can_BasedOn) {
		No_Show_Can_BasedOn = no_Show_Can_BasedOn;
	}
	public String getNo_Show_Can_Value() {
		return No_Show_Can_Value;
	}
	public void setNo_Show_Can_Value(String no_Show_Can_Value) {
		No_Show_Can_Value = no_Show_Can_Value;
	}
	
	

}
