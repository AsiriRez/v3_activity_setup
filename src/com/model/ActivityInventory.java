package com.model;

public class ActivityInventory {
	
	public String Activity_Program;
	public String Region;
	public String Tour_Operator;
	public String Contract_from;
	public String Contract_to;
	public String Activity_Period;
	public String Inventory_By;
	public String Total_Inventory;
	public String Cut_off;
	
	public String Friday_Inventory;
	public String Saturday_Inventory;
	public String Sunday_Inventory;
	
	public String getFriday_Inventory() {
		return Friday_Inventory;
	}
	public void setFriday_Inventory(String friday_Inventory) {
		Friday_Inventory = friday_Inventory;
	}
	public String getSaturday_Inventory() {
		return Saturday_Inventory;
	}
	public void setSaturday_Inventory(String saturday_Inventory) {
		Saturday_Inventory = saturday_Inventory;
	}
	public String getSunday_Inventory() {
		return Sunday_Inventory;
	}
	public void setSunday_Inventory(String sunday_Inventory) {
		Sunday_Inventory = sunday_Inventory;
	}
	
	
	public String getActivity_Program() {
		return Activity_Program;
	}
	public void setActivity_Program(String activity_Program) {
		Activity_Program = activity_Program;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getTour_Operator() {
		return Tour_Operator;
	}
	public void setTour_Operator(String tour_Operator) {
		Tour_Operator = tour_Operator;
	}
	public String getContract_from() {
		return Contract_from;
	}
	public void setContract_from(String contract_from) {
		Contract_from = contract_from;
	}
	public String getContract_to() {
		return Contract_to;
	}
	public void setContract_to(String contract_to) {
		Contract_to = contract_to;
	}
	public String getActivity_Period() {
		return Activity_Period;
	}
	public void setActivity_Period(String activity_Period) {
		Activity_Period = activity_Period;
	}
	public String getInventory_By() {
		return Inventory_By;
	}
	public void setInventory_By(String inventory_By) {
		Inventory_By = inventory_By;
	}
	public String getTotal_Inventory() {
		return Total_Inventory;
	}
	public void setTotal_Inventory(String total_Inventory) {
		Total_Inventory = total_Inventory;
	}
	public String getCut_off() {
		return Cut_off;
	}
	public void setCut_off(String cut_off) {
		Cut_off = cut_off;
	}
	
	

	
	
}
