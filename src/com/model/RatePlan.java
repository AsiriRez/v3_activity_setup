package com.model;

public class RatePlan {

	public String Rate_Plan = "";
	public String Min_person = "";
	public String Max_person = "";
	public String Rate_calculation = "";
	public String RatePlan_desc = "";
	
	public String getRate_Plan() {
		return Rate_Plan;
	}
	public void setRate_Plan(String rate_Plan) {
		Rate_Plan = rate_Plan;
	}
	public String getMin_person() {
		return Min_person;
	}
	public void setMin_person(String min_person) {
		Min_person = min_person;
	}
	public String getMax_person() {
		return Max_person;
	}
	public void setMax_person(String max_person) {
		Max_person = max_person;
	}
	public String getRate_calculation() {
		return Rate_calculation;
	}
	public void setRate_calculation(String rate_calculation) {
		Rate_calculation = rate_calculation;
	}
	public String getRatePlan_desc() {
		return RatePlan_desc;
	}
	public void setRatePlan_desc(String ratePlan_desc) {
		RatePlan_desc = ratePlan_desc;
	}
	
	
	
	
}
