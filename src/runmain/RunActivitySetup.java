package runmain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import activitysetup.ActivityBasicInfo;
import activitysetup.ActivitySetupInfo;
import activitysetup.PG_Properties;

import com.controller.ExcelReader;
import com.controller.LoadBasicDetails;
import com.controller.LoadDetails;
import com.model.*;


public class RunActivitySetup {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor)driver;	
	private LoadDetails detailsLoder;
	private LoadBasicDetails basicDetailsloader;
	private int i=0;
	
	ArrayList<Map<Integer, String>> Sheetlist = new ArrayList<Map<Integer, String>>(); 
	ArrayList<Map<Integer, String>> Sheetlist1 = new ArrayList<Map<Integer, String>>();
	ArrayList<ActivityStandard>  	ActivityStandardTypes  	= new ArrayList<ActivityStandard>();
	
	ArrayList<ProgramType>  	ProgramTypes  	= new ArrayList<ProgramType>();
	ArrayList<ActivityType>   	ActivityTypes	= new ArrayList<ActivityType>();	
	ArrayList<PeriodSetup>     	PeriodSetups    = new ArrayList<PeriodSetup>();
	ArrayList<RatePlan>     	RatePlans       = new ArrayList<RatePlan>();
	
	Map<Integer, String> ActivityStandardMap 	= null;
	Map<Integer, String> ContractMap 			= null;
	Map<Integer, String> PoliciesMap 			= null;
	Map<Integer, String> TaxMap 				= null;
	Map<Integer, String> AssignActivityMap 		= null;
	Map<Integer, String> ActivityInventoryMap 	= null;
	Map<Integer, String> ActivityRatesMap 		= null;
	Map<Integer, String> ActivityProfitMap 		= null;
	
	
	private TreeMap<String, ActivityStandard>   ActivityList;
	
	private StringBuffer PrintWriter;
	private int TestCaseCount;
	

	@Before
	public void setUp(){
		
		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
		driver = new FirefoxDriver(profile);
		PrintWriter = new StringBuffer();	
		
		ExcelReader Reader = new ExcelReader();		
		Sheetlist            = Reader.init(PG_Properties.getProperty("Excel.Path.FullActivity"));
		Sheetlist1           = Reader.init(PG_Properties.getProperty("Excel.Path.BasicInfo"));
		
		detailsLoder         = new LoadDetails();
		basicDetailsloader   = new LoadBasicDetails(Sheetlist1);
		
		ProgramTypes   	 	= basicDetailsloader.loadProgramType();
		ActivityTypes 		= basicDetailsloader.loadActivitySetup();
		PeriodSetups 		= basicDetailsloader.loadPeriodSetup();
		RatePlans 			= basicDetailsloader.loadRatePlans();

		ActivityStandardMap 		= Sheetlist.get(0);
		ContractMap					= Sheetlist.get(1);
		PoliciesMap					= Sheetlist.get(2);
		TaxMap						= Sheetlist.get(3);
		AssignActivityMap			= Sheetlist.get(4);
		ActivityInventoryMap		= Sheetlist.get(5);
		ActivityRatesMap			= Sheetlist.get(6);
		ActivityProfitMap			= Sheetlist.get(7);
		
		
		try {		
			ActivityList = detailsLoder.loadActivityStandard(ActivityStandardMap);
					
		} catch (Exception e) {					
			System.out.println(e.getMessage());			
		}
		    
		
		try {			
			ActivityList = detailsLoder.loadActivityContract(ContractMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {
			ActivityList = detailsLoder.loadActivityPolicies(PoliciesMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
			ActivityList = detailsLoder.loadActivityTax(TaxMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
				
		try {
			ActivityList = detailsLoder.loadActivityAssignActivity(AssignActivityMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {			
			ActivityList = detailsLoder.loadActivityInventories(ActivityInventoryMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {			
			ActivityList = detailsLoder.loadActivityRates(ActivityRatesMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {			
			ActivityList = detailsLoder.loadActivityProfits(ActivityProfitMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	@Test
	public void activitySetup() throws InterruptedException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
		ActivityBasicInfo activityBasicInfo = new ActivityBasicInfo(driver);
		ActivitySetupInfo activitySetupInfo = new ActivitySetupInfo(driver);
		
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Activity Setup Report - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		
		
		PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
		TestCaseCount = 1;
		
		//login
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
		PrintWriter.append("<td>Should login successfully</td>");
		
		if (activitySetupInfo.login(driver) == true) {
			PrintWriter.append("<td>Success..!!!</td>");
			PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
			
		} else {
			PrintWriter.append("<td>Login Errorrr..!!!!</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
		}
		
		
		
		//create activity basic info
		if (PG_Properties.getProperty("BasicInfoOperation").equals("Yes")) {
			
			//Programs
			/*try {				
				Iterator<ProgramType>  programIterator  = ProgramTypes.iterator();			
				while(programIterator.hasNext())
				{
					activityBasicInfo.createProgramType(programIterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			*/
			
			//Activity type
			try {				
				Iterator<ActivityType>  activityiterator  = ActivityTypes.iterator();			
				while(activityiterator.hasNext())
				{
					activityBasicInfo.createActivityType(activityiterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			
			//Periods
			try {				
				Iterator<PeriodSetup>  periodSetupIterator  = PeriodSetups.iterator();			
				while(periodSetupIterator.hasNext())
				{
					activityBasicInfo.createSetupPeriod(periodSetupIterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			
			//Rate Plans
			try {
				Iterator<RatePlan>  rateIterator  = RatePlans.iterator();			
				while(rateIterator.hasNext())
				{
					activityBasicInfo.createRatePlan(rateIterator.next());
				}
					
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
		}
		
		
		
		//create		
		if (PG_Properties.getProperty("ActivitySetupOperation").equals("Yes")) {
			
			Iterator<Map.Entry<String, ActivityStandard>> it = (Iterator<Entry<String, ActivityStandard>>) ActivityList.entrySet().iterator();
					
			while(it.hasNext())
			{
				
				ActivityStandard act = it.next().getValue();
				activitySetupInfo.addActivity(act);
				i++;
				
				TestCaseCount++;
				PrintWriter.append("<tr> <td>"+TestCaseCount+"</td>  <td>Create Activity program</td>");
				PrintWriter.append("<td>Activity program should be created</td>");
				
				try {
					
					driver = activitySetupInfo.createActivityStandard(driver);
					driver = activitySetupInfo.addContracts(driver);
					driver = activitySetupInfo.addPolicies(driver);
					driver = activitySetupInfo.addContent(driver);
					driver = activitySetupInfo.addImages(driver);
					driver = activitySetupInfo.assignActivities(driver);
					driver = activitySetupInfo.addActivityInventories(driver);
					driver = activitySetupInfo.addActivityRates(driver);
					driver = activitySetupInfo.addProfitMarkups(driver);
					
					
					PrintWriter.append("<td>Created Activity - "+act.getActivity_Program_Name()+" - Done</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				} catch (Exception e) {
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
			        
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			
			
		}
		
		//Delete
		if (PG_Properties.getProperty("DeleteOperation").equals("Yes")) {
				
			Iterator<Map.Entry<String, ActivityStandard>> it = (Iterator<Entry<String, ActivityStandard>>) ActivityList.entrySet().iterator();
			
			while(it.hasNext())
			{
				ActivityStandard act = it.next().getValue();
				i++;
				
				TestCaseCount++;
				PrintWriter.append("<tr> <td>"+TestCaseCount+"</td>  <td>Remove Activity program</td>");
				PrintWriter.append("<td>Activity program should be removed</td>");
				
				try {
					activitySetupInfo.deleteActivityStandard(act);
					PrintWriter.append("<td>Success..!!!</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				} catch (Exception e) {
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
			        
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			
			
		}
				
		PrintWriter.append("</table>");
	}
	
	@After
	public void tearDown() throws IOException{
		PrintWriter.append("</body></html>");
		  
		  BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("ActivityFullReport"))));
		  bwr.write(PrintWriter.toString());
		  bwr.flush();
		  bwr.close();
	      driver.quit();
	}

}
